__author__ = 'bezobiuk'

import math

COLOR_WHITE = 'w'
COLOR_BLACK = 'b'


def klass_piece():
    return Queen


class CoordinatesConverter(object):
    LITERAL_NUMBER = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8}


class ChessCoordinatesConverter(CoordinatesConverter):

    @staticmethod
    def literal_to_numbers(string_coordinates):  # перетворює координати із строкового виду в числовий
        return CoordinatesConverter.LITERAL_NUMBER[string_coordinates[0]], int(string_coordinates[1])

    @staticmethod
    def numbers_to_literal(coordinates):  # перетворює координати із числового виду в строковий
        return sorted(CoordinatesConverter.LITERAL_NUMBER)[coordinates[0]-1] + str(coordinates[1])


class Move(object):

    @staticmethod  # ПРАВИЛЬНО?????
    def to_create_move(element_for_move):  # створює обєкти класів ходів
        new_coordinate, operation_coordinate, klass, figure = element_for_move
        return klass(new_coordinate, operation_coordinate, figure)

    def to_make_move(self):
        pass

    def return_move(self):
        pass


class ChessMove(Move):

    def __init__(self, new_coordinates, operation_coordinates, figure):
        self.board = figure.board
        self.new_coordinates = new_coordinates
        self.operation_coordinates = operation_coordinates
        self.figure = figure
        self.start_old_coordinates = self.figure.old_coordinates
        self.removed_figure_data = []
        self.rook_start_coordinates = None

    def to_make_move(self):  # робить хід
        self.figure.old_coordinates = self.figure.coordinates
        self.figure.coordinates = self.new_coordinates
        self.board.moves.append(self)

    def return_move(self):  # повертає назад зроблений хід
        self.figure.coordinates = self.figure.old_coordinates
        self.figure.old_coordinates = self.start_old_coordinates
        self.board.moves.pop(-1)

    def return_needed_piece(self, coordinates_for_compare):  # повертає фігуру по заданих координатах
        for figure in self.board.pieces:
            if figure.coordinates == coordinates_for_compare:
                return figure

    def removed_piece_on_board(self, figure):  # б"є фігуру на дошці
        self.removed_figure_data.append((figure, self.board.pieces.index(figure)))
        self.board.pieces.remove(figure)

    def return_piece_on_board(self):  # повертає фігуру на дошку
        figure, index = self.removed_figure_data.pop(-1)
        self.board.pieces.insert(index, figure)

    def create_piece_on_board(self, klas_pieces):  # створює фігуру на дошці
        self.board.pieces.append(klas_pieces(self.board, self.board.size_x, self.board.size_y, self.new_coordinates,
                                             self.figure.color, None))

    def delete_piece_on_board(self, figure):  # удаляє фігуру з дошки
        self.board.pieces.remove(figure)


class StandardMove(ChessMove):
    pass


class ToRemoveMove(ChessMove):

    def to_make_move(self):
        figure = self.return_needed_piece(self.new_coordinates)
        self.removed_piece_on_board(figure)
        super().to_make_move()

    def return_move(self):
        super().return_move()
        self.return_piece_on_board()


class CastlingMove(ChessMove):

    def to_make_move(self):
        rook_x, new_rook_x = self.operation_coordinates
        for figure in self.board.pieces:
            if figure.coordinates == rook_x:
                self.rook_start_coordinates = figure.coordinates
                figure.coordinates = new_rook_x
        super().to_make_move()

    def return_move(self):
        rook_x, new_rook_x = self.operation_coordinates
        super().return_move()
        for figure in self.board.pieces:
            if figure.coordinates == new_rook_x:
                figure.coordinates = self.rook_start_coordinates
                self.rook_start_coordinates = rook_x


class TunnelMove(ChessMove):

    def to_make_move(self):
        figure = self.return_needed_piece(self.operation_coordinates)
        self.removed_piece_on_board(figure)
        super().to_make_move()

    def return_move(self):
        super().return_move()
        self.return_piece_on_board()


class ChangePawnMove(ChessMove):

    def to_make_move(self):
        self.create_piece_on_board(klass_piece())
        self.removed_piece_on_board(self.figure)

    def return_move(self):
        self.return_piece_on_board()
        figure = self.return_needed_piece(self.new_coordinates)
        self.delete_piece_on_board(figure)


class ChangePawnRemoveMove(ChessMove):
    def to_make_move(self):
        figure = self.return_needed_piece(self.new_coordinates)
        self.removed_piece_on_board(figure)
        self.create_piece_on_board(klass_piece())
        self.removed_piece_on_board(self.figure)

    def return_move(self):
        figure = self.return_needed_piece(self.new_coordinates)
        self.delete_piece_on_board(figure)
        self.return_piece_on_board()
        self.return_piece_on_board()


class Pieces(object):

    def __init__(self, board, size_x, size_y,  coordinates, color, old_coordinates):
        self.old_coordinates = old_coordinates
        self.coordinates = coordinates
        self.color = color
        self.board = board
        self.size_x = size_x
        self.size_y = size_y

# в get_possible_moves(self, terms=None) змінна terms=None по замовчуванню, а передається вона тільки в двох випадках,
# коли треба не проводити перевірку короля на можливість рокіровки, тому що там також треба можливі ходи, в яких теж
# була перевірка на рокіровку, і програма заходила в рекурсію.
# self.square_is_attacked(self.coordinates, self.condition)  condition = 'kings_possible_moves_free_of_castling'
# condition передається в верхню функцію, бо вона працює з функцією приведеною нижче:
# def method_for_all_pieces(self, method, condition, method_condition, terms=None):
#        return [getattr(p, method)(terms) for p in self.pieces if condition == getattr(p, method_condition)]
# тому коли при вирахуванні можливих ходів короля, під час вирахування можливості рокіровки, коли програма перевіряє чи
# окремі клітинки не стають під боєм, то перевірка на рокіровку короля протилежного кольору не проводим,
# бо цей його хід не вплине на можливість проведення рокіровки короля даного кольору, а програма зайде в рекурсію.

    def real_possible_moves(self):  # залишає всі можливі ходи після перевірки на шах
        pass

    def get_possible_moves(self, terms=None):  # можливі ходи фігури
        pass

    def previous_coordinate(self):  # повертає координати попереднього ходу фігури
        return self.old_coordinates

    def i_am_here(self, coordinates):  # зрівнює координати з своїми
        return self.coordinates == coordinates

    def piece_on_square(self, coordinates, attribute):  # відповідає на запитання чи фігура на клітинці
        return self.board.get_piece(coordinates, attribute)

    def inside(self, x, y):  # перевіряє виход координати за розмір дошки
        return (self.size_x >= x >= 1) and (self.size_y >= y >= 1)


class ChessPieces(Pieces, ChessCoordinatesConverter):
    COLOR = 'color'
    OLD_COORD = 'old_coordinates'
    POSSIBLE_MOVES = 'get_possible_moves'
    operations = tuple()
    iteration_type = None
    short_name = None

    def check_to_the_king(self):  # перевіряє чи король під шахом
        for piece in self.board.pieces:
            if piece.short_name == 'K' and piece.color == self.color:
                if self.square_is_attacked(piece.coordinates, None):
                    return True

    def standard_iteration(self, operation, possible_moves):  # Проводить стандартні маніпуляції над координатами
        x, y = self.literal_to_numbers(self.coordinates)  # для більшості шахматних фігур
        delta_x, delta_y = operation
        while True:
            if self.inside(x + delta_x, y + delta_y):
                coordinates_new_square = self.numbers_to_literal((x + delta_x, y + delta_y))
                pieces_color_on_square = self.piece_on_square(coordinates_new_square, self.COLOR)
                if not pieces_color_on_square:
                    possible_moves.append((coordinates_new_square, '', StandardMove, self))
                    x, y = x + delta_x, y + delta_y
                    continue
                elif pieces_color_on_square:
                    if pieces_color_on_square != self.color:
                        possible_moves.append((coordinates_new_square, '', ToRemoveMove, self))
            break
        return possible_moves

    def secondary_iteration(self, operation, possible_moves):  # Проводить стандартні маніпуляції над координатами
        x, y = self.literal_to_numbers(self.coordinates)  # для деяких шахматних фігур
        delta_x, delta_y = operation
        if self.inside(x + delta_x, y + delta_y):
            coordinates_new_square = self.numbers_to_literal((x + delta_x, y + delta_y))
            pieces_color_on_square = self.piece_on_square(coordinates_new_square, self.COLOR)
            if not pieces_color_on_square:
                possible_moves.append((coordinates_new_square, '', StandardMove, self))
            elif pieces_color_on_square and pieces_color_on_square != self.color:
                possible_moves.append((coordinates_new_square, '', ToRemoveMove, self))
        return possible_moves

    def get_possible_moves(self, terms=None):  # обчислює координати можливих ходів фігур по заданих алгоритмах
        possible_moves = []
        for operation in self.operations:
            possible_moves = self.standard_iteration(operation, possible_moves) if self.iteration_type == 0\
                else self.secondary_iteration(operation, possible_moves)
        return possible_moves

    def real_possible_moves(self):  # обчислює реальні можливі ходи фігури, які не приводять до шаху
        # print(self.get_possible_moves())  # всі прінти в цій функції тільки для наглядності процесу
        real_possible_moves = []
        for i in self.get_possible_moves(None):
            move_piece = Move().to_create_move(i)
            move_piece.to_make_move()
            # print('атрибуты после хода', (self.coordinates, self.color, self.short_name, self.old_coordinates,\
            # move_piece.removed_figure_data, move_piece.rook_start_coordinates))
            # print('доска после хода', move_piece.board.pieces)
            # print('список ходов после хода', move_piece.board.moves)
            if not self.check_to_the_king():
                real_possible_moves.append(i)
            move_piece.return_move()
            # print('атрибуты после возврата хода', (self.coordinates, self.color, self.short_name,\
            # self.old_coordinates, move_piece.removed_figure_data, move_piece.rook_start_coordinates))
            # print('доска после возврата хода', move_piece.board.pieces)
            # print('список ходов после возврата хода', move_piece.board.moves)
            del move_piece  # щоб не загаджувати, так???????????!!!!!!!!!!
        return real_possible_moves

    def square_is_attacked(self, coordinates, terms):  # перевіряє чи клітинка з даними координатами під боєм
        color = COLOR_WHITE if self.color == COLOR_BLACK else COLOR_BLACK
        for moves in self.board.method_for_all_pieces(self.POSSIBLE_MOVES, color, self.COLOR, terms):
            for move in moves:
                if move[0] == coordinates:
                    return True
        return False


class Pawn(ChessPieces):

    short_name = 'P'
    WHITE_DIRECTION = 1
    BLACK_DIRECTION = -1
    WHITE_Y = 2
    BLACK_Y = 7
    WHITE_TUNNEL_Y = 5
    BLACK_TUNNEL_Y = 4

    def get_possible_moves(self, terms=None):  # обчислює координати можливих ходів пішки
        possible_moves = self.get_attacked_moves()
        x, y = self.literal_to_numbers(self.coordinates)
        direction = self.WHITE_DIRECTION if self.color == COLOR_WHITE else self.BLACK_DIRECTION
        delta_x, delta_y = (0, direction)
        if self.inside(x + delta_x, y + delta_y):
            coordinates_new_square = self.numbers_to_literal((x + delta_x, y + delta_y))
            pieces_color_on_square = self.piece_on_square(coordinates_new_square, self.COLOR)
            if not pieces_color_on_square:
                if (y == 7 and self.color == COLOR_WHITE) or (y == 2 and self.color == COLOR_BLACK):
                    possible_moves.append((coordinates_new_square, '', ChangePawnMove, self))
                else:
                    possible_moves.append((coordinates_new_square, '', StandardMove, self))
                y_compare = self.WHITE_Y if self.color == COLOR_WHITE else self.BLACK_Y
                if y == y_compare:
                    delta_x, delta_y = (0, 2 * direction)
                    coordinates_new_square = self.numbers_to_literal((x + delta_x, y + delta_y))
                    pieces_color_on_square = self.piece_on_square(coordinates_new_square, self.COLOR)
                    if not pieces_color_on_square:
                        possible_moves.append((coordinates_new_square, '', StandardMove, self))
        return possible_moves

    def get_attacked_moves(self):  # вираховує можливі ходи пішака з боєм і з боєм на проході
        possible_moves = []
        x, y = self.literal_to_numbers(self.coordinates)
        direction = self.WHITE_DIRECTION if self.color == COLOR_WHITE else self.BLACK_DIRECTION
        operation = [(1, direction), (-1, direction)]
        for delta_x, delta_y in operation:
            if self.inside(x + delta_x, y + delta_y):
                coordinates_new_square = self.numbers_to_literal((x + delta_x, y + delta_y))
                pieces_color_on_square = self.piece_on_square(coordinates_new_square, self.COLOR)
                if pieces_color_on_square and pieces_color_on_square != self.color:
                    if (y == 7 and self.color == COLOR_WHITE) or (y == 2 and self.color == COLOR_BLACK):
                        possible_moves.append((coordinates_new_square, '', ChangePawnRemoveMove, self))
                    else:
                        possible_moves.append((coordinates_new_square, '', ToRemoveMove, self))
                y_compare = self.WHITE_TUNNEL_Y if self.color == COLOR_WHITE else self.BLACK_TUNNEL_Y
                if y == y_compare:
                    d_x, d_y = (delta_x, 2) if self.color == COLOR_WHITE else (delta_x, -2)
                    check_x, check_y = x + d_x, y
                    if self.inside(check_x, check_y):
                        coordinates_check = self.numbers_to_literal((check_x, check_y))
                        color_for_compare = self.piece_on_square(coordinates_check, self.COLOR)
                        if color_for_compare and color_for_compare != self.color:
                            last_moved_piece = self.board.return_last_piece()
                            coordinates_old_square = self.numbers_to_literal((x + d_x, y + d_y))
                            if last_moved_piece and last_moved_piece.short_name == self.short_name and\
                                    last_moved_piece.previous_coordinate() == coordinates_old_square:
                                possible_moves.append((coordinates_new_square, coordinates_check, TunnelMove, self))
        return possible_moves


class Rook(ChessPieces):

    short_name = 'R'   # атрібути для обчислення списка можливих ходів
    operations = [(-1, 0), (1, 0), (0, 1), (0, -1)]
    iteration_type = 0


class Knight(ChessPieces):

    short_name = 'N'
    operations = [(-1, 2), (1, 2), (2, 1), (2, -1), (1, -2), (-1, -2), (-2, -1), (-2, 1)]
    iteration_type = 1


class Bishop(ChessPieces):

    short_name = 'B'
    operations = [(1, 1), (1, -1), (-1, -1), (-1, 1)]
    iteration_type = 0


class Queen(ChessPieces):

    short_name = 'Q'
    operations = [(-1, 0), (1, 0), (0, 1), (0, -1), (1, 1), (1, -1), (-1, -1), (-1, 1)]
    iteration_type = 0


class King(ChessPieces):

    condition = 'kings_possible_moves_free_of_castling'
    short_name = 'K'
    operations = [(-1, 0), (1, 0), (0, 1), (0, -1), (1, 1), (1, -1), (-1, -1), (-1, 1)]
    castling_operations = ((3, 2, 1), (-4, -2, -1))
    iteration_type = 1

    def get_possible_moves(self, terms=None):  # обчислює можливі ходи короля
        possible_moves = super().get_possible_moves()
        if terms is not None:
            return possible_moves
        x, y = self.literal_to_numbers(self.coordinates)
        if not self.square_is_attacked(self.coordinates, self.condition):
            for delta_x, increment_x, delta_rook_x in self.castling_operations:
                coordinate_for_check = self.numbers_to_literal((x + delta_x, y))
                if self.piece_on_square(self.coordinates, self.COLOR) and\
                    not self.piece_on_square(self.coordinates, self.OLD_COORD) and\
                    self.piece_on_square(coordinate_for_check, self.COLOR) and\
                        not self.piece_on_square(coordinate_for_check, self.OLD_COORD):
                    indicator = True
                    for d_x in range(1, int(math.fabs(delta_x))):  # тут частини схожі, але різні і винести
                        if self.piece_on_square(self.numbers_to_literal(  # окремо геморойно
                                (x + int(math.copysign(d_x, delta_x)), y)), self.COLOR):  # математична хрінь щоб
                            indicator = False   # зберегти напрямок та знак даних для проведення перевірки
                    for d_x in range(1, int(math.fabs(increment_x))):
                        if self.square_is_attacked(self.numbers_to_literal((x + int(math.copysign
                                                                            (d_x, increment_x)), y)), self.condition):
                            indicator = False
                    if indicator:
                        possible_moves.append((self.numbers_to_literal((x + increment_x, y)),
                                               (self.numbers_to_literal((x + delta_x, y)),
                                                self.numbers_to_literal((x + delta_rook_x, y))), CastlingMove, self))
        return possible_moves


class Board(object):
    PIECES = {}

    def __init__(self, size_x, size_y):
        self.moves = []
        self.pieces = []
        self.size_x = size_x
        self.size_y = size_y

    def start_board(self):  # генерує стартову позицію
        for klass, value in self.PIECES.items():
            for square in value:
                self.pieces.append(klass(self, self.size_x, self.size_y, *square))

    def get_piece(self, coordinates, attribute):  # вибирає фігуру на дошці, питає чи вона на клітинці
        for piece in self.pieces:      # з заданими координатами
            if piece.i_am_here(coordinates):  # і повертає її заданий атрибут, або None
                return getattr(piece, attribute)

    def method_for_all_pieces(self, method, condition, method_condition, terms=None):  # визиває метод для фігури
        return [getattr(p, method)(terms) for p in self.pieces if condition == getattr(p, method_condition)]

    def return_last_piece(self):  # повертає фігуру, яка остання ходила
        #return self.moves[-1].figure if self.moves else None
        return Pawn(s, s.size_x, s.size_y, 'e5', COLOR_BLACK, 'e7')  # тільки для перевірки бою на проході


class ChessBoard(Board):

    PIECES = {
        Rook:
        [('a1', COLOR_WHITE, None), ('h1', COLOR_WHITE, None), ('a8', COLOR_BLACK, None), ('h8', COLOR_BLACK, None)],
        Knight:
        [('b1', COLOR_WHITE, None), ('g1', COLOR_WHITE, None), ('b8', COLOR_BLACK, None), ('g8', COLOR_BLACK, None)],
        Bishop:
        [('c1', COLOR_WHITE, None), ('f1', COLOR_WHITE, None), ('c8', COLOR_BLACK, None), ('f8', COLOR_BLACK, None)],
        Queen:
        [('d1', COLOR_WHITE, None), ('d8', COLOR_BLACK, None)],
        King:
        [('e1', COLOR_WHITE, None), ('e8', COLOR_BLACK, None)],
        Pawn:
        [('a2', COLOR_WHITE, None), ('b2', COLOR_WHITE, None), ('c2', COLOR_WHITE, None), ('d2', COLOR_WHITE, None),
         ('e2', COLOR_WHITE, None), ('f2', COLOR_WHITE, None), ('g2', COLOR_WHITE, None), ('h2', COLOR_WHITE, None),
         ('a7', COLOR_BLACK, None), ('b7', COLOR_BLACK, None), ('c7', COLOR_BLACK, None), ('d7', COLOR_BLACK, None),
         ('e7', COLOR_BLACK, None), ('f7', COLOR_BLACK, None), ('g7', COLOR_BLACK, None), ('h7', COLOR_BLACK, None)]
    }

    def __init__(self):
        super().__init__(8, 8)
        self.start_board()


s = ChessBoard()
s.pieces = [
          King(s, s.size_x, s.size_y, 'e8', COLOR_BLACK, None), Rook(s, s.size_x, s.size_y, 'a1', COLOR_WHITE, None),
          Bishop(s, s.size_x, s.size_y, 'g3', COLOR_BLACK, None), Pawn(s, s.size_x, s.size_y, 'b2', COLOR_BLACK, None),
          Pawn(s, s.size_x, s.size_y, 'd5', COLOR_WHITE, 'd4'), Pawn(s, s.size_x, s.size_y, 'e5', COLOR_BLACK, 'e7'),
          Rook(s, s.size_x, s.size_y, 'a8', COLOR_BLACK, None), King(s, s.size_x, s.size_y, 'e2', COLOR_WHITE, None),
          Rook(s, s.size_x, s.size_y, 'h8', COLOR_BLACK, None), Rook(s, s.size_x, s.size_y, 'h1', COLOR_WHITE, None)]
print(s.pieces)
print(s.moves)
for piece in s.pieces:
    print(piece.real_possible_moves())